package it.lundstedt.erik;
import com.opencsv.*;

import java.io.*;
import java.util.List;

public class reader
{
	String dir;

	public reader(String dir) {
		this.dir = dir;
	}

	public String[] readCsvLine(int line) {
		return getStrings(line, dir);
	}

	static String[] readCsvLine(int line,String dir) {
		return getStrings(line, dir);
	}

	private static String[] getStrings(int line, String dir) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(dir));
			List<String[]> lines = reader.readAll();
			String[] lineContents = lines.get(line);
			return lineContents;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		String[] err={"error"};
		return err;
	}




}
